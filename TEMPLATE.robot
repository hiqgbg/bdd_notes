*** Settings ***
Documentation       Use case headline
...
...                 *As a* [X]
...
...                 *I want* [Y]
...
...                 *so that* [X]


*** Test Cases ***

Acceptance Criteria: Some determinable business situation
    Given some precondition
      and some other precondition
    When some action by the user
     and some other action
    Then some testable outcome is archived
     and something else that can be checked happens too

*** Keywords ***

some precondition
    No Operation

some other precondition
    No Operation

some action by the user
    No Operation

some other action
    No Operation

some testable outcome is archived
    No Operation

something else that can be checked happens too
    No Operation
