*** Settings ***
Documentation       Add new note
...
...                 *As a* user
...
...                 *I want* add a new note
...
...                 *so that* i can recall what happend later

Library             SeleniumLibrary

*** Test Cases ***

Acceptance Criteria: There should be a field to enter a note into
    Given system is running
    When user comes to the start page
    Then there should be an text field on the page

Acceptance Criteria: Text entered in the field should be persistant
    Given there is text in the input field
    When the user reloads the page
    Then the text should still be in the field


*** Keywords ***

system is running
    Wait Until Page Contains  Notes  timeout=30s

user comes to the start page
    Go To   ${TOP_URL}
    Wait Until Page Contains  Notes  timeout=30s

there should be an text field on the page
    Capture Page Screenshot
    Page Should Contain Element  class:quill

there is text in the input field
    Go To   ${TOP_URL}
    Wait Until Page Contains  Notes  timeout=30s
    Input Text  ql-editor-1  Hello Me
    Capture Page Screenshot

the user reloads the page
    Reload Page
    Wait Until Page Contains  Notes  timeout=30s

the text should still be in the field
    Capture Page Screenshot
    Element Should Contain  ql-editor-1  Hello Me


