*** Settings ***
Documentation       List all your notes in the sidebar
...
...                 *As a* user
...
...                 *I want* to have a overview of all my notes
...
...                 *so that* i can easily find them all

Library             SeleniumLibrary

*** Test Cases ***

Acceptance Criteria: The current note should be visible in the sidebar
    Given I have an empty note
    When I add a head line
    Then The head line should apper in the sidebar

*** Keywords ***

I have an empty note
    Wait Until Page Contains  Notes  timeout=30s

I add a head line
    Input Text  ql-editor-1  Hello Me

The head line should apper in the sidebar
    Element Should Contain  sidebar  Hello Me
    
