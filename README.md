BDD with Robot Framework
========================

This repo contains an example note taking application with
acceptance tests in Behaivoud Driven Design (BDD) style implemented
in Robot Framework.


Running
=======

Requirements:

  * Docker
  * Docker compose

Tested on Ubuntu 18.04, e.g. Docker version 17.12.1-ce, docker-compose version 1.17.1


Build
-----

    docker-compose -f verification-docker-compose.yaml build

Start test application on port 3001

    docker-compose -f verification-docker-compose.yaml up -d application

Open browser to http://localhost:3001/ to view page

To run tests with chrome:

    docker-compose -f verification-docker-compose.yaml run --rm -e HOST_UID=$(id -u) -e HOST_GID=$(id -g) robot --name System --variablefile libraries/chrome.py design environment
